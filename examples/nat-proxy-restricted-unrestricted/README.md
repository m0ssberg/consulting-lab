Example: Restricted, Unrestricted, and NAT networks.
====================================================

This example is meant to show how you can create several networks of all the different types, and place several VM's on them along with `port_openings:` on a network, vm and global basis.  


The [inventory/labenv](inventory/labenv) models up the whole environment.

In the end we will get something that looks like this

```
                  |-FW-|net1(10.101.0.0/24|-------
                  |             |
                  |             |- host1 (10.101.0.22)
                  |
                  |-FW-|net2(10.102.0.0/24)|
                  |             |
                  |             |- host2(10.102.0.23)
                  |              
                  |-FW-|mgmt(10.5.0.0/24)|
                  |             |
  labhost eth0 ---|             |- dns1(10.5.0.2)
                  |             |- dns2(10.5.0.3)     
                  |
                  |----open-net1 (10.140.0.0/24)
                  |             |
                  |             |- open-host1 (10.140.0.10)
                  |
                  |----nat1 (10.200.0.0/24)
                                |
                                |- proxy (10.200.0.20)
                                |
                                |- nat-host1 (10.200.0.10)
                                |- nat-host2 (10.200.0.11)
```
- All vm's are running RHEL 7.6 based on the template created with `vm-template`.
- The networks `net1`,`net2` and `mgmt` are restricted (`-FW-|`). `port_openings:` on the networks, vm's  and global (port_openings: dictate what is opened.  
- The networks `open-net1` _and_ `nat1` are unrestricted.
- Network `nat1` is of type _nat_, so every host here can reach the internet. 

By network defined we mean these:
```
# inventory/labenv
    libvirt_networks:
    - name: mgmt
      type: restricted
      subnet: 10.5.0.0/24
      port_openings:       <--------
      - port: 53
        proto: tcp
        comment: "allow DNS from all networks"
      - port: 53
        proto: udp
        comment: "allow DNS from all networks"
      - port: 389
        proto: tcp
        comment: "allow LDAP from all networks"
      - port: 636
        proto: tcp
        comment: "allow LDAPS from all networks"
      - port: 88
        proto: tcp
        comment: "allow Kerberos from all networks"
      - port: 464
        proto: tcp
        comment: "allow Kerberos from all networks"
```

By VM defined we mean these:  
```
    vms:
      hosts:
        mgmt-host:
          name: "mgmt-host"
          cpu: "1" 
          memory: "2048"
          network: "mgmt"
          ip_address: "10.5.0.21"
          port_openings:     <-----------
          - port: 1234
            proto: udp
            src: "10.101.0.0/24"
            comment: "Allow whole net1 on port 1234/udp"

```

By global we mean these:
```
    # Firewall
    port_openings:
    - port: 22
      proto: tcp
      chain: any-to-any
      src: "10.5.0.21"
      comment: "allow SSH to any from mgmt-host"
```

Usage
-----

To try this example out you first have to fill in your user information in `inventory/labenv`.  
This will be propagated to all VM's and used to configure them.  
It simplifies things if you go with your regular pub key in `.ssh/id_rsa.pub`. 
```
    users:
    - name: yourusername
      public_key: "ssh-rsa YOURPUBLICCKEYTHATYOUUSE"
``` 
and you also need to supply some valid RH credentials:
The lab requires this in multiple places. 

``` 
    rh_username: youraccount@redhat.com
    rh_password: ***********
```



With that in you are ready to go.  
Boot your hetzner host into rescue mode and run the playbook.
```
$ cd examples/nat-proxy-restricted-unrestricted/
$ ansible-playbook setup_lab.yml -i inventory/

```
The run will take ~40 mins to complete.  

_**If there should be any errors along the way.. most of the stuff is "idempotent" that you can run it again..**_  
- `vm-template` will fail if it has run and there already is an image with the same name.  
- `create-inventory-vms` will not create any vm's that can be detected with `virsh list`.  
- `hetzner-provision` will simply do nothing if it's not in rescue mode. 



                 
