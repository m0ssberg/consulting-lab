Labhost libvirt-iptables
================

NOTE: This role have clear cut dependency to the [libvirt-network](/roles/libvirt-network/) role.
It doesn't make sense to run this without first running libvirt-network. 

What is this?
-------------

[[_TOC_]] 

One key-concept with the consulting-lab is that we want to simulate a typical customer network.  
There are typically restricted by default rather than wide open. If a port needs opening, we have to specify which port on which network, and from where.  

In our Lab, the labhost is the _gateway_ and firewall. All virtual network traffic goes through the labhost.  
So to control what is allowed and not allowed between the networks we have to interact with netfilter on the labhost.  
For this, we use **iptables** and not firewalld which is the default on RHEL 7.  


This role will configure our different network **types**:
```
  libvirt_networks:
  - name: my-restricted-net
    type: restricted     <---
    subnet: 10.0.0.0/24

```

The different types are:
- **restricted** (default):  
  Network is locked down by default. No ports are opened from outside the network.  
  You have to specifically allow certain ports using the `port_openings:` dict (read more here []()).  
  This is the default network type and will be used if you don't specify `network.type:`.   

- **unrestricted**:  
  Network is wide-open for all other networks. All ports are available from all other networks.  

- **nat**:
  NAT'ed network. All hosts on this network are [MASQUARADED]() and have access to the WAN link.  
  Acts as _unrestricted_ network, meaning all hosts on all other networks can access any port on this network.  
  Useful for putting your http-proxy ([squid](/roles/squid/)) or other resources that requires direct access to Internet.  




How it works
------------

`libvirt-network` creates all the virtual networks and the `virbr-` bridge interfaces using the [open-network]() type.  
This means basically that no iptables FORWARDING rules are applied. It's just a blank network, not even routable.  

We are mostly used to deal with the more common libvirt network types such as [routed](), [nat]() and [isolated]().  
The reason for going with open network is that we want more control over what is allowed and what is not.  
As the aim here is to simulate a typical customer network where it's typically whitelisting (restricted) rather than wide open, with a lot of added port openings.  
It can be debated if this type of setup is really necessary, but as of right now no other option has not been presented. (libvirt do have [NetFilter](https://libvirt.org/formatnwfilter.html) however).  

The role will read of all the defined `libvirt_networks` and apply set of rules for each network.  
Depending on the `type:` on the network, different rules apply.  

### Chains 

We make heavy use of chains.  
For every network created in `libvirt_networks:` we create a `to-$network.name` chain and a matching jump rule in the FORWARD chain.  
Basically, if any package have the outgoing interface of `virbr-$network.name` it should go to the `to-$network.name` chain.

```

```

By having `to-network` chains, we can more easily group, control the _ordering_ of the rules and add changes that should be applied on a network basis.  
It's **important** to remember that these chain only affects the _incoming_ traffic to the network.  
We allow _any_ RELATED or ESTABLISHED network traffic regardless if it's a restricted on unrestricted network.   

An example, where we have two networks: 
- **net1** 10.1.0.0/24  
- **net2** 10.2.0.0/24  

Our inventory file looks like this:  
```
    libvirt_networks:
     - name: net1
       type: restricted
       subnet: 10.1.0.0/24

     - name: net2
       type: restricted
       subnet: 10.2.0.0/24
```
After we run the libvirt-network and  libvirt-iptables setup
```
$ ansible-playbook adhoc/create-networks.yml -i inventory/
``` 

.. we get the following:

```
# iptables -L FORWARD -v
Chain FORWARD (policy DROP 0 packets, 0 bytes)
 pkts bytes target     prot opt in     out     source               destination         
   61 16445 ACCEPT     all  --  any    any     anywhere             anywhere             ctstate RELATED,ESTABLISHED
   85  7068 any-to-any  all  --  any    any     anywhere             anywhere             /* Catch all for more global rules */
    0     0 to-net1    all  --  any    virbr-net1  anywhere             anywhere             /* match all traffic to net1 (restricted) */
    0     0 to-net2    all  --  any    virbr-net2  anywhere             anywhere             /* match all traffic to net2 (restricted) */
```

We can see the two rules with `target: to-net1` and `target: to-net2`, and we can see that the matching happens on the `out:` interface.  
The targets in this output are the chains that have been created.  
_(`any-to-any` will be covered further down)_

```
iptables -L  to-net1 -v
Chain to-net1 (1 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 ACCEPT     all  --  virbr-net1 virbr-net1  anywhere             anywhere             /* Allow inter-LAN traffic on net1 */
```
```
iptables -L  to-net2 -v
Chain to-net2 (1 references)
 pkts bytes target     prot opt in     out     source               destination         
    0     0 ACCEPT     all  --  virbr-net2 virbr-net2  anywhere             anywhere             /* Allow inter-LAN traffic on net2 */
```

We see that the respective chains only contain one rule, with the comment `/* Allow inter-LAN traffic on net2 */`.  
This rule is to ensure that traffic within the LAN will be routed. i.e. 10.1.0.100 can reach 10.1.0.200.  

Other than that, there are no more rules!  
These are restricted networks, so nothing else but inter-LAN traffic will be allowed.  
Note here the target/jump of the rule: `ACCEPT`.  
If the rule were written on the command line it would look like this:
```
# All network entering on NIC virbr-net2 and leaving on the same NIC virbr-net2 will go to ACCEPT
iptables -A to-net1 -i virbr-net2 -o virbr-net2 -j ACCEPT
``` 
For other traffic, that is not inter-LAN, we rely on the FORWARD chains default policy `DROP` to make sure the packages are not forwarded.  
This is the inner workings of iptables.  

When we match the package with an outgoing interface of `virbr-net1`, we will tell it to jump to the chain `to-net1`.
The package "enters" the chain `to-net1`.  
If there is no matching rule that will terminate (-j jump) the package, it will return to the chain it came from, being the FORWARD chain.  

We try to visualize it with a package that has left the PREROUTING(?) phase and enters the FORWARD chain.
It comes from a host on `net1` with a destination to a host on `net2`. The outbound interface for `virbr-net2`.
_(note any-to-any is not included in this flow)_   
```

Package enters the FORWARD chain.
     |
     |
 is the package related or established? - NO
     |
     |
 is the package destined for output nic virbr-net1? - NO
     |
     |
 is the package destined for output nic virbr-net2? - YES
     \
      --
        \  * enter chain to-net2
        |
        | does the package have input interface virbr-net2? - NO 
        |
        | * no termination, go back to FORWARD chain
        /
      --
     /
     |
     |
    DROP (default)
```

If it were a inter-LAN package; a package coming in on interface `virbr-net2` and output interface `virbr-net2`, it would be picked up by the matching rule in the `to-net2` chain.  

```
Package enters the FORWARD chain.
     |
     |
 is the package related or established? - NO
     |
     |
 is the package destined for output nic virbr-net1? - NO
     |
     |
 is the package destined for output nic virbr-net2? - YES
     \
      --
        \  * enter chain to-net2
        |
        | does the package have input interface virbr-net2? - YES
        \
         \ -j ACCEPT 
```
### Port openings on restricted networks

When we want to allow a package on a specific network, we add the port opening to the specific `to-network` chain.  
Lets say we have a webserver on `webnet` with an ip of `10.10.0.100`, serving on port 80/tcp and 443/tcp.  

This server needs to be accessed from our `monnet` where we have some monitoring solution on ip `10.50.0.40`.  
```
 monitoring.lab.com (10.50.0.50) --------> :80,:443 webserver.lab.com 10.10.0.100
```

We allow this with the following rule: 
```
$ iptables -A to-webnet --src 10.50.0.40 --dst 10.10.0.100 -m tcp -p tcp --dport 80 -j ACCEPT
$ iptables -A to-webnet --src 10.50.0.40 --dst 10.10.0.100 -m tcp -p tcp --dport 443 -j ACCEPT
```

This shouldn't be news to many of us, with the exception that we do the rule addition to a chain.  
Now, there is nothing stopping us from inserting this at the top (or bottom) of the FORWARD chain and it would still work.. but we would loose the organization and track of what rules belong to which.  

#### Network wide openings

We can also create wider rules by omitting a `--src`, which in effect would mean `any`.  
This would open up the port to any network that can route to this network.  

```
$ iptables -A to-webnet --dst 10.10.0.100 -m tcp -p tcp --dport 80 -j ACCEPT
$ iptables -A to-webnet --dst 10.10.0.100 -m tcp -p tcp --dport 443 -j ACCEPT
```

To take it further, and open these ports for the while network, simply omit `--dst`.

```
$ iptables -A to-webnet -m tcp -p tcp --dport 80 -j ACCEPT
$ iptables -A to-webnet -m tcp -p tcp --dport 443 -j ACCEPT

```

#### Automate (define in inventory)
Since we want to be able to automate and desribe our labenv in config, this role will pick up any `port_openings:`.
We can attach the `port_openings:` list directly in the inventory or to both networks and host entries. 

The role checks for a dictionary containing the following:  
```
  port: port number 
  proto: tcp/udp
  chain: to-netX
  dst: (optional) ipv4 address or subnet
  src: (optional) ipv4 address or subnet
  comment: (optional) Rule comment 
``` 

As we will see, depending on where you define the `port_openings:` list, the role can figure out some of the entries by itself.


##### network defined openings

When we define the networks, we can simply add a list of port openings to the network:

```
$ cat inventory/labenv
  ---
all:
  vars:
    libvirt_networks:
    - name: webnet
      subnet: 10.10.0.0/24
      type: restricted
      port_openings:
      - port: 80
        proto: tcp
        src: 10.50.0.40
        dst: 10.10.0.100
  
      - port: 443
        proto: tcp
        src: 10.50.0.40
        dst: 10.10.0.100
  
    - name: monnet
      subnet: 10.50.0.40/24
      type: restricted
    
```
It will read them one by one and add them to the `to-chain` for that network. 

Here you can also omit the `src:` or `dst:` and the role will treat it as above. i.e. not adding the `--src` and `--dst` making the rule wider.  

```
$ cat inventory/labenv
  ---
all:
  vars:
    libvirt_networks:
    - name: webnet
      subnet: 10.10.0.0/24
      type: restricted
      port_openings:
      
      # allow any host on any network to access 10.10.0.100 on port 80
      - port: 80
        proto: tcp
        dst: 10.10.0.100

    
```
```
$ cat inventory/labenv
  ---
all:
  vars:
    libvirt_networks:
    - name: webnet
      subnet: 10.10.0.0/24
      type: restricted
      port_openings:
      
      # allow host 10.10.0.100 to access any host within webnet  on port 80
      - port: 80
        proto: tcp
        src: 10.10.0.100

```

##### host defined openings 

You can also do this on the VM entries! See [vm-create](/roles/vm-create).  
The port opening will pick up the VM's `vm.network` and use it fo map to the correct _to-net_ chain, aswell as the  `vm.ip_address` and use it as `dst:`.
So We can omit the `dst:` here as it will be the ip of the VM.  

**NOTE:** `src:` will still be needed in order to **not** open it up to wide (if thats what you want).   

```
  #####################
  # Virtual Machines
  #####################
  children:
    vms:
      hosts:
        mysql:
          name: "mysql"
          cpu: "1"
          memory: "2048"
          network: "net2"
          ip_address: "10.102.0.10"
          port_openings:
          - port: 3306
            proto: tcp
            src: 10.10.0.100
            comment: "allow webserver to access db"

        webserver:
          name: "webserver"
          cpu: "1"
          memory: "2048"
          network: "net2"
          ip_address: "10.10.0.100"
          port_openings:
          - port: 443 
            proto: tcp
            comment: "allow 443 from everywhere"
            
```
##### globally defined port openings

We can also specify the port openings directly in the inventory, but it requires us to specify which `chain:` should be used.  
All chains except the `any-to-any` that we should be interested in have the `to-net` format.  

The previous example above can be written like this instead:

```
all:
  vars:
  ---
    port_openings:
    - port: 3306
      proto: tcp
      src: 10.10.0.100
      dst: 10.102.0.10
      chain: to-net2
      comment: "allow webserver to access db"

    - port: 433
      proto: tcp
      dst: 10.10.0.100
      chain: to-net2
      comment: "allow 443 from everywhere"

```


### Global rules and (any-to-any) 

Sometimes we want add rules that are not tied to an input network and are more overreaching.  
For instance we may want to enforce certain rules across all networks, and we don't want to specify them for all networks one-by-one.
This is particularly useful for when we want to enforce something from _one_ network to all the others.  
Basically, we want a rule that has a `src:` but no `dst:`.  

The first example that comes to mind is SSH.  
Say we want to make sure that we can ssh to anywhere within the infra from a certain host, like an Ansible Tower or a jumpserver.  
We want an iptables rule that would look like this:
```
iptables -I FORWARD ? --src tower.example.com --dst 0.0.0.0/0 -m tcp -p tcp --dport 22 -m comment --comment "Tower can ssh anywhere"
```
WIP:
```

#
# port_openings:
# - port: 22
#   proto: ssh
#   chain: any-to-any
#   src: 10.101.0.5
#   comment: "Allow ssh everywhere from Tower"




# This is to pick up any port openings defined outside of the
# libvirt_networks: and vms: lists.
# 
# These rules does not come with a preset to-net chain or a dst.
# It requires you to specify all mandatory variables in the port_opening: dict
#
#   port: 
#   proto:
#   chain: 
#   dst: (optional)
#   src: (optional)
#   comment: (optional)
#
# We can use the special chain any-to-any, which comes at the top of the FORWARD chain
# meaning it will be parsed first.
# Here we can allow for some more freeform rules that aren't tied to any network.
# For instance, we can make a global sweep across all the networks allowing SSH on every network
#
# port_openings:
# - port: 22
#   proto: ssh
#   chain: any-to-any
#   comment: "Allow ssh everywhere"
#
# We can fine tune it a bit and say that SSH is allowed on all networks if it comes from a certain source.
# Useful for say Ansible Tower or a jumpserver
#
# port_openings:
# - port: 22
#   proto: ssh
#   chain: any-to-any
#   src: 10.101.0.5
#   comment: "Allow ssh everywhere from Tower"
```
