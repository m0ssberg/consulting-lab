Labhost vm-template
===================

This role will create an up to date RHEL7.6 image that can be used as an template for new VM's.
It's created using the RHEL 7 boot.iso fetched using the [rhel-iso-download](/roles/rhel-iso-download).  
The boot.iso requires a rhel7 yum repository. We provide this using a [`vm-template-repoproxy`](/roles/vm-template-repoproxy).   

The process is fully automated with a Kickstart [file](/roles/vm-template/templates/rhel.ks).  
You can ofcourse make tunings in this file, but consider configuring your clones with Ansible instead, described more in [vm-create](/roles/vm-create).  


The image should be prepared with the users supplied with the [`users` role](/roles/users/) in the kickstart.
So if you've supplied a `public_key:` for your user, it should be in the user authorized_keys, meaning that you can SSH to your clone right away and you can configure it using Ansible.


Usage
-----

Checkout the [adhoc playbook](/adhoc/create-vm-template.yml) of how to run the role or the multiple [examples](/examples/)  

The role requires that [`subman`](/roles/subman/) is completed so we are entitled yo use CDN resources.  
